package parser.Utils;

import org.eclipse.emf.ecore.EObject;

public class EObjectUtils {
    public static String getItemId(EObject eObject) {
        return eObject.eResource().getURIFragment(eObject);
    }
}
