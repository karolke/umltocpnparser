package parser.Entities;

import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import parser.Containters.EHashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InteractionType {
    private String name;
    private EHashMap<Lifeline> lifeLines;
    private EHashMap<MessageOccurrenceSpecification> messageSpecifications;
    private EHashMap<Message> messages;

    public InteractionType(String name) {
        this.name = name;
        lifeLines = new EHashMap<>();
        messageSpecifications = new EHashMap<>();
        messages = new EHashMap<>();
    }

    public void addLifeLine(Lifeline lifeLine) {
        lifeLines.put(lifeLine);
    }

    public void addMessageSpecification(MessageOccurrenceSpecification spec) {
        messageSpecifications.put(spec);
    }

    public void addMessage(Message message) {
        messages.put(message);
    }

    public String getName() {
        return name;
    }

    public EHashMap<Lifeline> getLifeLines() {
        return lifeLines;
    }

    public EHashMap<MessageOccurrenceSpecification> getMessageSpecifications() {
        return messageSpecifications;
    }

    public EHashMap<Message> getMessages() {
        return messages;
    }

}
