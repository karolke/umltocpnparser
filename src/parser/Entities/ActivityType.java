package parser.Entities;

import org.eclipse.uml2.uml.*;
import parser.Containters.EHashMap;
import parser.Utils.EObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ActivityType {
    private String name;
    private InitialNode initialNode;
    private String initialNodeId;
    private String finalNodeId;
    private ActivityFinalNode finalNode;
    private EHashMap<Action> actionList;
    private EHashMap<DecisionNode> decisionNodes;
    private List<EdgeType> edgeList;
    private boolean edgesGenerated = false;

    public ActivityType(String name) {
        this.name = name;
        actionList = new EHashMap<>();
        decisionNodes = new EHashMap<>();
        edgeList = new ArrayList<>();
    }

    public InitialNode getInitialNode() {
        return initialNode;
    }

    public String getInitialNodeId() {
        return initialNodeId;
    }

    public List<EdgeType> getEdgeList() {
        return edgeList;
    }

    public EHashMap<Action> getActionList() {
        return actionList;
    }

    public void setInitialNode(InitialNode initialNode) {
        this.initialNode = initialNode;
        this.initialNodeId = EObjectUtils.getItemId(initialNode);
    }

    public ActivityFinalNode getFinalNode() {
        return finalNode;
    }

    public String getFinalNodeId() {
        return finalNodeId;
    }

    public void setFinalNode(ActivityFinalNode finalNode) {
        this.finalNode = finalNode;
        this.finalNodeId = EObjectUtils.getItemId(finalNode);
    }

    public void addAction(Action action) {
        actionList.put(action);
    }

    public void addDecision(DecisionNode decisionNode) {
        decisionNodes.put(decisionNode);
    }

    public void addEdge(ControlFlow edge) {
        edgeList.add(new EdgeType(EObjectUtils.getItemId(edge.getSource()),EObjectUtils.getItemId(edge.getTarget()),EdgeType.prepareName(edge)));
    }

    public void convertDecisionToEdges() {
        if(!edgesGenerated) {
            List<EdgeType> sourceEdges;
            List<EdgeType> targetEdges;
            int counter = 1;
            for (Map.Entry<String, DecisionNode> decisionBlock : decisionNodes.entrySet()) {
                sourceEdges = new ArrayList<>();
                targetEdges = new ArrayList<>();

                //Find sources and targets
                for (EdgeType edge : edgeList) {
                    if (edge.getSource().equals(decisionBlock.getKey())) {
                        targetEdges.add(edge);
                    }
                    if (edge.getTarget().equals(decisionBlock.getKey())) {
                        sourceEdges.add(edge);
                    }
                }

                //Make new edges
                for (EdgeType source : sourceEdges) {
                    for (EdgeType target : targetEdges) {
                        edgeList.add(new EdgeType(source.getSource(),target.getTarget(),target.getName()));
                    }
                }

                //Remove old edges
                for (EdgeType edge : sourceEdges) {
                    edgeList.remove(edge);
                }
                for (EdgeType edge : targetEdges) {
                    edgeList.remove(edge);
                }
            }
            edgesGenerated = true;
        }
    }
}
