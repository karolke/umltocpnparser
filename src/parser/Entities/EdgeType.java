package parser.Entities;

import org.eclipse.uml2.uml.ControlFlow;

public class EdgeType {
    private String source;
    private String target;
    private String name;
    private static int customTransitionCounter = 1;

    public EdgeType(String source, String target, String name) {
        this.source = source;
        this.target = target;
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    public String getName() {
        return name;
    }

    public static String prepareName(ControlFlow edge){
        if(edge.getName() != null)
            return edge.getName();
        else if(edge.getSource().getName() != null && edge.getTarget().getName() != null)
            return edge.getSource().getName() + "_to_" + edge.getTarget().getName();
        else
            return "Transition_" + customTransitionCounter++;
    }
}
