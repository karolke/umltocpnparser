package parser.Containters;

import org.eclipse.emf.ecore.EObject;
import parser.Utils.EObjectUtils;
import java.util.LinkedHashMap;

public class EHashMap<K extends EObject> extends LinkedHashMap<String,K> {
    public void put(K item) {
        put(EObjectUtils.getItemId(item),item);
    }
}
