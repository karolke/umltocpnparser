package parser.Enums;

public enum ArcDirection {
    PLACE_TO_TRANSITION("PlaceToTransition"),
    TRANSITION_TO_PLACE("TransitionToPlace");

    final String direction;

    ArcDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return direction;
    }
}
